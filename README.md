| :exclamation:  DO NOT INSTALL OR RUN THIS CODE   |
|--------------------------------------------------|
**This code repository may have insecure code within it.  This is by design for testing purposes for the WMF Security Team.**

---

This is a TEST repository for the following project:

https://gitlab.wikimedia.org/repos/security/gitlab-ci-security-templates

https://www.mediawiki.org/wiki/Security/Application_Security_Pipeline
