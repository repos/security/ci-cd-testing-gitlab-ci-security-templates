#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import hashlib
import requests
import sys


""" eval w/ dynamic data """
""" python-bandit, semgrep """
if len(sys.argv) > 0:
    eval(sys.argv[1])

""" requests.get w/out timeout """
""" python-bandit """
r = requests.get("https://example.com/")

""" weak encruption (md5) """
""" python-bandit """
hashlib.new('md5')

""" try w/ pass """
""" python-bandit """
try:
    a = 1
except:
    pass
