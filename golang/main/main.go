package main

import (
	"fmt"
	"html/template"
	"math/big"
	"os"
)

const tmpl = ""

func main() {

	// golang-gosec
	password := "f62e5bcda4fae4f82370da0c6f20697b8f8447ef"
	fmt.Println(password)

	// golang-gosec
	r := big.Rat{}
	r.SetString("13e-9223372036854775808")
	fmt.Println(r)

	// golang-gosec
	a := "something from another place"
	t := template.Must(template.New("ex").Parse(tmpl))
	v := map[string]interface{}{
		"Title": "Test <b>World</b>",
		"Body":  template.JS(a),
	}
	t.Execute(os.Stdout, v)
}
